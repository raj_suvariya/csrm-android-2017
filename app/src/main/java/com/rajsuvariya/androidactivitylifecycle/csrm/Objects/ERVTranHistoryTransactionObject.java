package com.rajsuvariya.androidactivitylifecycle.csrm.Objects;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kuliza-319 on 3/2/17.
 */

public class ERVTranHistoryTransactionObject implements Parcelable {

    private String activity;
    private String pointStatus;
    private int pointsTreats;

    public ERVTranHistoryTransactionObject(String activity, String pointStatus, int pointsTreats) {
        this.activity = activity;
        this.pointStatus = pointStatus;
        this.pointsTreats = pointsTreats;
    }

    protected ERVTranHistoryTransactionObject(Parcel in) {
        activity = in.readString();
        pointStatus = in.readString();
        pointsTreats = in.readInt();
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getPointStatus() {
        return pointStatus;
    }

    public void setPointStatus(String pointStatus) {
        this.pointStatus = pointStatus;
    }

    public int getPointsTreats() {
        return pointsTreats;
    }

    public void setPointsTreats(int pointsTreats) {
        this.pointsTreats = pointsTreats;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(activity);
        dest.writeString(pointStatus);
        dest.writeInt(pointsTreats);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public final Creator<ERVTranHistoryTransactionObject> CREATOR = new Parcelable.Creator<ERVTranHistoryTransactionObject>() {
        @Override
        public ERVTranHistoryTransactionObject createFromParcel(Parcel in) {
            return new ERVTranHistoryTransactionObject(in);
        }

        @Override
        public ERVTranHistoryTransactionObject[] newArray(int size) {
            return new ERVTranHistoryTransactionObject[size];
        }
    };
}
