package com.rajsuvariya.androidactivitylifecycle.csrm;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import com.rajsuvariya.androidactivitylifecycle.csrm.Interfaces.NavHeaderCommunicator;

/**
 * Created by kuliza-319 on 6/2/17.
 */

public class BaseActivity extends AppCompatActivity implements NavHeaderCommunicator{

    private DrawerLayout mDrawerLayoutLeft;
    private FrameLayout mFrameLayout;
    private boolean isLogin;
    Toolbar mToolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        mDrawerLayoutLeft = (DrawerLayout) findViewById(R.id.base_drawer_layout);
        mFrameLayout = (FrameLayout) findViewById(R.id.frame_base_activity_content);
        mToolbar = (Toolbar) findViewById(R.id.toolbar_base_activity);
        mToolbar.setTitle("");
        setSupportActionBar(mToolbar);

        closeNavDrawer(GravityCompat.END);
        closeNavDrawer(GravityCompat.START);

    }

    @Override
    public void closeNavDrawer(int gravity) {
        mDrawerLayoutLeft.closeDrawer(gravity);
    }

    public void disableNavDrawer(int gravity){
        mDrawerLayoutLeft.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, gravity);
    }

    public void enableNavDrawer(int gravity){
        mDrawerLayoutLeft.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_OPEN, gravity);
    }

    public void setLeftActionBarIcon(int imageId){
        if (getSupportActionBar()==null)
            throw new NullPointerException("getSupoprtActionBar BaseActivity");
        getSupportActionBar().setHomeAsUpIndicator(imageId);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public View setChildContentView(int layoutId){
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(layoutId, null, false);
        mFrameLayout.addView(contentView, 0);
        return contentView;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (isUserLoggedIn()) {
            getMenuInflater().inflate(R.menu.base, menu);
        }
        return true;
    }

    public boolean isUserLoggedIn(){
        SharedPreferences pref = this.getSharedPreferences("CSRMPref", MODE_PRIVATE);
        return pref.getBoolean("isUserLoggedIn", false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home){
            if (mDrawerLayoutLeft.isDrawerOpen(GravityCompat.START)){
                mDrawerLayoutLeft.closeDrawer(GravityCompat.START);
            }else {
                mDrawerLayoutLeft.openDrawer(GravityCompat.START);
            }
        }
        if (item.getItemId()==R.id.action_account_details){
            if (mDrawerLayoutLeft.isDrawerOpen(GravityCompat.END)){
                mDrawerLayoutLeft.closeDrawer(GravityCompat.END);
            }else {
                mDrawerLayoutLeft.openDrawer(GravityCompat.END);
            }
        }
        return super.onOptionsItemSelected(item);
    }

}
