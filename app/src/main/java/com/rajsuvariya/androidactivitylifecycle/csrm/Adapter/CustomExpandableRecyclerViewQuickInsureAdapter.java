package com.rajsuvariya.androidactivitylifecycle.csrm.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.rajsuvariya.androidactivitylifecycle.csrm.Objects.ERVQuickInsuranceObject;
import com.rajsuvariya.androidactivitylifecycle.csrm.R;
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by kuliza-319 on 2/2/17.
 */

public class CustomExpandableRecyclerViewQuickInsureAdapter extends ExpandableRecyclerViewAdapter<ExpandableRecyclerViewQuickInsureGroupViewHolder, ExpandableRecyclerViewQuickInsureChildViewHolder> {

    public CustomExpandableRecyclerViewQuickInsureAdapter(List list) {
        super(list);
    }

    @Override
    public ExpandableRecyclerViewQuickInsureGroupViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View groupView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_expandable_quick_insurance_group_layout, parent, false);
        return new ExpandableRecyclerViewQuickInsureGroupViewHolder(groupView);
    }

    @Override
    public ExpandableRecyclerViewQuickInsureChildViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View childView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_expandable_quick_insurance_item_layout, parent, false);
        return new ExpandableRecyclerViewQuickInsureChildViewHolder(childView);
    }

    @Override
    public void onBindChildViewHolder(ExpandableRecyclerViewQuickInsureChildViewHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {
        final ERVQuickInsuranceObject insuranceObject = ((ERVQuickInsuranceObject) group.getItems().get(childIndex));
        holder.setData(insuranceObject);
    }

    @Override
    public void onBindGroupViewHolder(ExpandableRecyclerViewQuickInsureGroupViewHolder holder, int flatPosition, ExpandableGroup group) {
        holder.setGenreTitle(group);
    }
}

class ExpandableRecyclerViewQuickInsureChildViewHolder extends ChildViewHolder{

    @BindView(R.id.iv_card_view_quick_insurance_banner) ImageView mImageViewBanner;
    @BindView(R.id.iv_card_view_quick_insurance_title) TextView mTextViewTitle;
    @BindView(R.id.iv_card_view_quick_insurance_description) TextView mTextViewDescription;
    @BindView(R.id.bt_card_view_quick_insurance_button) View mViewCustomButtonWrapper;
    private Button mButtonCustomButton;

    public ExpandableRecyclerViewQuickInsureChildViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        mButtonCustomButton = (Button) mViewCustomButtonWrapper.findViewById(R.id.custombutton_layout_button);
    }

    public void setData(ERVQuickInsuranceObject insurance) {
        mTextViewTitle.setText(insurance.getInsuranceTitle());
        mTextViewDescription.setText(insurance.getInsuranceDescription());
        mImageViewBanner.setImageResource(insurance.getInsuranceImageId());
        mButtonCustomButton.setText(insurance.getInsuranceButtonText());
    }
}

class ExpandableRecyclerViewQuickInsureGroupViewHolder extends GroupViewHolder{

    private TextView genreTitle;
    @BindView(R.id.iv_erv_quick_insurance_group_layout_down_arrow) ImageView mDownArrow;
    @BindView(R.id.tv_erv_quick_insurance_group_layout_title) TextView mTitle;


    public ExpandableRecyclerViewQuickInsureGroupViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void setGenreTitle(ExpandableGroup group) {
        mTitle.setText(group.getTitle());
    }

    @Override
    public void expand() {
        mDownArrow.animate().rotation(180);
    }

    @Override
    public void collapse() {
        mDownArrow.animate().rotation(0);
    }
}
