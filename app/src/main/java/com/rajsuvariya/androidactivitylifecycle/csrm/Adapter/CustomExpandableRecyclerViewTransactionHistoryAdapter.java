package com.rajsuvariya.androidactivitylifecycle.csrm.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.rajsuvariya.androidactivitylifecycle.csrm.Objects.ERVTranHistoryTransactionObject;
import com.rajsuvariya.androidactivitylifecycle.csrm.R;
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by kuliza-319 on 2/2/17.
 */

public class CustomExpandableRecyclerViewTransactionHistoryAdapter extends ExpandableRecyclerViewAdapter<ExpandableRecyclerViewGroupViewHolder, ExpandableRecyclerViewChildViewHolder> {

    public CustomExpandableRecyclerViewTransactionHistoryAdapter(List list) {
        super(list);
    }

    @Override
    public ExpandableRecyclerViewGroupViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View groupView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_expandable_transaction_history_group_layout, parent, false);
        return new ExpandableRecyclerViewGroupViewHolder(groupView);
    }

    @Override
    public ExpandableRecyclerViewChildViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View childView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_expandable_transaction_history_item_layout, parent, false);
        return new ExpandableRecyclerViewChildViewHolder(childView);
    }

    @Override
    public void onBindChildViewHolder(ExpandableRecyclerViewChildViewHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {
        final ERVTranHistoryTransactionObject transaction = ((ERVTranHistoryTransactionObject) group.getItems().get(childIndex));
        holder.setData(transaction);

    }

    @Override
    public void onBindGroupViewHolder(ExpandableRecyclerViewGroupViewHolder holder, int flatPosition, ExpandableGroup group) {
        holder.setGenreTitle(group);
    }
}

class ExpandableRecyclerViewChildViewHolder extends ChildViewHolder{

    @BindView(R.id.tv_transaction_item_activity) TextView mTextViewActivity;
    @BindView(R.id.tv_transaction_item_points_status) TextView mTextViewPointsStatus;
    @BindView(R.id.tv_transaction_item_points_treats) TextView mTextViewPointsTreats;

    public ExpandableRecyclerViewChildViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void setData(ERVTranHistoryTransactionObject transaction) {
        mTextViewActivity.setText(transaction.getActivity());
        mTextViewPointsStatus.setText(transaction.getPointStatus());
        mTextViewPointsTreats.setText(transaction.getPointsTreats()+"");
    }
}

class ExpandableRecyclerViewGroupViewHolder extends GroupViewHolder{

    private TextView genreTitle;
    @BindView(R.id.iv_expandable_recycler_view_group_layout_down_arrow) ImageView imageView;
    @BindView(R.id.tv_expandable_recycler_view_group_layout_date) TextView textView;


    public ExpandableRecyclerViewGroupViewHolder (View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void setGenreTitle(ExpandableGroup group) {
        textView.setText(group.getTitle());
    }

    @Override
    public void expand() {
        imageView.animate().rotation(180);
    }

    @Override
    public void collapse() {
        imageView.animate().rotation(0);
    }
}
