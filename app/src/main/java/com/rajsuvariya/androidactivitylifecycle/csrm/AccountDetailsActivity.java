package com.rajsuvariya.androidactivitylifecycle.csrm;

import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.rajsuvariya.androidactivitylifecycle.csrm.Adapter.CustomExpandableRecyclerViewTransactionHistoryAdapter;
import com.rajsuvariya.androidactivitylifecycle.csrm.Objects.ERVTranHistoryGroupObject;
import com.rajsuvariya.androidactivitylifecycle.csrm.Objects.ERVTranHistoryTransactionObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AccountDetailsActivity extends BaseActivity {

    @BindView(R.id.rl_account_details_transcation_history_title) View mViewTransactionHistoryTitle;
    @BindView(R.id.rl_account_details_transcation_history) View mViewTransactionHistory;
    @BindView(R.id.iv_account_details_down_arrow) ImageView mImageViewDownArrow;
    @BindView(R.id.rv_account_details_transaction_history) RecyclerView mRecyclerViewTransactionHistory;
    @BindView(R.id.tv_account_details_name) TextView mTextViewACHolderName;
    @BindView(R.id.tv_account_details_email) TextView mTextViewACHolderEmail;
    @BindView(R.id.tv_account_details_dob) TextView mTextViewACHolderDob;
    @BindView(R.id.tv_account_details_mobile) TextView mTextViewACHolderMobile;
    @BindView(R.id.tv_account_details_address) TextView mTextViewACHolderAddress;
    @BindView(R.id.tv_account_details_gold_card_name) TextView mTextViewGoldCardACHolderName;
    @BindView(R.id.tv_account_details_gold_card_membership_id) TextView mTextViewGoldCardMembershipID;
    @BindView(R.id.tv_account_details_gold_card_points) TextView mTextViewGoldCardPoints;
    @BindView(R.id.tv_account_details_tran_history_card_points) TextView mTextViewTranHistoryTotalPoints;
    @BindView(R.id.bt_account_details_tran_history_filter) TextView mTextViewTranHistoryFiler;
    @BindView(R.id.bt_account_details_tran_history_sort) TextView mTextViewTranHistorySort;

    @BindView(R.id.iv_account_details_right_arrow) ImageView mImageViewTranHistoryNextArrow;
    @BindView(R.id.iv_account_details_left_arrow) ImageView mImageViewTranHistoryPrevArrow;

    @BindView(R.id.tv_account_details_gold_card_redeem_button) View mViewGoldCardRedeemButton;
    @BindView(R.id.bt_account_details_recommended_offer_know_more) View mViewRecommendedOfferKnowMoreButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View contentView = super.setChildContentView(R.layout.activity_account_details);
        super.setLeftActionBarIcon(R.drawable.icon_menu);
        super.closeNavDrawer(GravityCompat.END);
        super.closeNavDrawer(GravityCompat.START);


        ButterKnife.bind(this, contentView);

        ((Button) mViewGoldCardRedeemButton.findViewById(R.id.custombutton_layout_button)).setText(getString(R.string.button_redeem_now_label));
        ((Button) mViewRecommendedOfferKnowMoreButton.findViewById(R.id.custombutton_layout_button)).setText(getString(R.string.button_know_more_label));

        mViewTransactionHistoryTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mViewTransactionHistory.getVisibility()==View.VISIBLE){
                    mViewTransactionHistory.setVisibility(View.GONE);
                    mImageViewDownArrow.animate().rotation(0).start();
                }
                else{
                    mViewTransactionHistory.setVisibility(View.VISIBLE);
                    mImageViewDownArrow.animate().rotation(180).start();
                }
            }
        });


        List<ERVTranHistoryGroupObject> go = new ArrayList<>();
        List<ERVTranHistoryTransactionObject> li = new ArrayList<>();
        li.add(new ERVTranHistoryTransactionObject("da", "dsa", 500));
        go.add(new ERVTranHistoryGroupObject("01-02-2017", li));
        go.add(new ERVTranHistoryGroupObject("10-02-2017", li));

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        CustomExpandableRecyclerViewTransactionHistoryAdapter adapter = new CustomExpandableRecyclerViewTransactionHistoryAdapter(go);
        mRecyclerViewTransactionHistory.setLayoutManager(layoutManager);
        mRecyclerViewTransactionHistory.setAdapter(adapter);

    }

}

