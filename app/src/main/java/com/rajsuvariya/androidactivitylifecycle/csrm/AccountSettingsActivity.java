package com.rajsuvariya.androidactivitylifecycle.csrm;

import android.support.design.widget.TabLayout;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AccountSettingsActivity extends BaseActivity {

    @BindView(R.id.tv_account_settings_account_details_label) TextView mTextViewAcDetailsLabel;
    @BindView(R.id.tv_account_settings_reset_password_label) TextView mTextViewResetPassLabel;
    @BindView(R.id.tl_account_settings_account_details) TableLayout mTableLayoutShowAcDetails;
    @BindView(R.id.ll_account_settings_ac_edit_details) LinearLayout mLinearLayoutEditAcDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View rootView = super.setChildContentView(R.layout.activity_account_settings);
        super.setLeftActionBarIcon(R.drawable.icon_menu);

        ButterKnife.bind(this, rootView);

        mTextViewAcDetailsLabel.setText("Ds");
        mTextViewAcDetailsLabel.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (mTextViewAcDetailsLabel.getRight() - mTextViewAcDetailsLabel.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        mTableLayoutShowAcDetails.setVisibility(View.GONE);
                        mLinearLayoutEditAcDetails.setVisibility(View.VISIBLE);
                        return true;
                    }
                }
                return true;
            }
        });
    }
}
