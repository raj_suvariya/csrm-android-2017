package com.rajsuvariya.androidactivitylifecycle.csrm.Objects;

import com.rajsuvariya.androidactivitylifecycle.csrm.AccountDetailsActivity;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

/**
 * Created by kuliza-319 on 3/2/17.
 */

public class ERVTranHistoryGroupObject extends ExpandableGroup<ERVTranHistoryTransactionObject> {

    public ERVTranHistoryGroupObject(String title, List<ERVTranHistoryTransactionObject> items) {
        super(title, items);
    }


}