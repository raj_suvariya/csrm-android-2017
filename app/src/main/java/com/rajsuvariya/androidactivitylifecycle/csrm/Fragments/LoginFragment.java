package com.rajsuvariya.androidactivitylifecycle.csrm.Fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.rajsuvariya.androidactivitylifecycle.csrm.Helper.Message;
import com.rajsuvariya.androidactivitylifecycle.csrm.HomeActivity;
import com.rajsuvariya.androidactivitylifecycle.csrm.MainActivity;
import com.rajsuvariya.androidactivitylifecycle.csrm.R;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by kuliza-319 on 20/1/17.
 */

public class LoginFragment extends Fragment {

    @BindView(R.id.login_username_wrapper) TextInputLayout userNameWrapper;
    @BindView(R.id.login_password_wrapper) TextInputLayout passwordWrapper;
    @BindView(R.id.login_otp_wrapper) TextInputLayout otpWrapper;
    @BindView(R.id.login_resetpass_newpass) TextInputLayout newPassWrapper;
    @BindView(R.id.login_resetpass_repeatpass) TextInputLayout repeatPassWrapper;
    @BindView(R.id.login_bt_boarderless) Button btBoarderless;
    @BindView(R.id.login_tv_reset_pass_label) TextView tvResetPassLabel;
    @BindView(R.id.login_tv_didnotgetotp_label) TextView tvDidnotgetOTPLabel;
    @BindView(R.id.login_layout_custom_button) View btCustomButtonLayout;
    @BindView(R.id.custombutton_layout_button) Button btCustomButtonTextButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);

        // Binding views with ButterKnife
        ButterKnife.bind(this, rootView);

        // Setting texhwatchers for all input field
        userNameWrapper.getEditText().addTextChangedListener(new MainActivity.MyTextWatcher(userNameWrapper));
        passwordWrapper.getEditText().addTextChangedListener(new MainActivity.MyTextWatcher(passwordWrapper));
        newPassWrapper.getEditText().addTextChangedListener(new MainActivity.MyTextWatcher(newPassWrapper));
        repeatPassWrapper.getEditText().addTextChangedListener(new MainActivity.MyTextWatcher(repeatPassWrapper));
        otpWrapper.getEditText().addTextChangedListener(new MainActivity.MyTextWatcher(otpWrapper));

        // Main Button (i.e., Login, Reset, Submit) on click listener
        btCustomButtonLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // When password field is visible then user is on Login page
                if(passwordWrapper.getVisibility()==View.VISIBLE) {
                    boolean isUsernameEmpty = TextUtils.isEmpty(userNameWrapper.getEditText().getText().toString());
                    boolean isPasswordEmpty = TextUtils.isEmpty(passwordWrapper.getEditText().getText().toString());
                    if (isUsernameEmpty) { // User haven't entered username and click icon_next so show error
                        userNameWrapper.setError(getString(R.string.error_no_username_entered));
                    }
                    if (isPasswordEmpty) { // User haven't entered password and click icon_next so show error
                        passwordWrapper.setError(getString(R.string.error_no_password_entered));
                    }
                    if (!(isPasswordEmpty || isUsernameEmpty)) { // API call to check credentials
                        Message.showShortSnack(v, "Perform Action"); // Just message to developer
                        SharedPreferences pref = getContext().getSharedPreferences("CSRMPref", MODE_PRIVATE);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putBoolean("isUserLoggedIn", true);
                        editor.commit();

                        Intent i = new Intent(getContext(), HomeActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                    }
                }
                // if New Password field is visible then user is on password reset page
                else if (newPassWrapper.getVisibility()==View.VISIBLE){
                    boolean isNewPassEmpty = TextUtils.isEmpty(newPassWrapper.getEditText().getText().toString());
                    boolean isRepeatPassEmpty = TextUtils.isEmpty(repeatPassWrapper.getEditText().getText().toString());
                    if(isNewPassEmpty){ // User haven't entered new password and click icon_next so show error
                        newPassWrapper.setError(getString(R.string.error_no_password_entered));
                    }
                    if (isRepeatPassEmpty){ // User haven't confirmed password and click icon_next so show error
                        repeatPassWrapper.setError(getString(R.string.error_no_password_entered));
                    }
                    if(!(isNewPassEmpty|isRepeatPassEmpty)){
                        // Both the passwords match call the API to complete the process
                        if(newPassWrapper.getEditText().getText().toString().equals(repeatPassWrapper.getEditText().getText().toString()))
                            Message.showShortSnack(v, "Password Reset Successfull"); // Just message to developer
                        // Passwords do not match and user click icon_next so show error
                        else
                            repeatPassWrapper.setError(getString(R.string.error_passwords_do_not_match));
                    }
                }
                // When OTP field is not visible means user is yet to request OTP
                else if(otpWrapper.getVisibility()==View.GONE){
                    boolean isUsernameEmpty = TextUtils.isEmpty(userNameWrapper.getEditText().getText().toString());
                    if (isUsernameEmpty) {  // User haven'e entered username and click icon_next so show error
                        userNameWrapper.setError(getString(R.string.error_incorrect_customer_id));
                    }
                    else{ // validate username and send OTP, call the API for both the actions
                        Message.showShortSnack(v, "OTP Sent"); // Just message to developer
                        otpWrapper.setVisibility(View.VISIBLE);
                        btBoarderless.setVisibility(View.VISIBLE);
                        tvDidnotgetOTPLabel.setVisibility(View.VISIBLE);
                        btBoarderless.setText(R.string.otp_send_again_button_text);
                        userNameWrapper.setEnabled(false);
                    }
                }
                // When OTP field is visible means user have received OTP he yet haven't entered OTP
                else if(otpWrapper.getVisibility()==View.VISIBLE){
                    boolean isOTPEmpty = TextUtils.isEmpty(otpWrapper.getEditText().getText().toString());
                    if (isOTPEmpty){  // User haven't entered OTP and click icon_next so show error
                        otpWrapper.setError(getString(R.string.error_no_otp_entered));
                    }
                    // User haven't entered correct OTP (i.e., 4 DIGITS) and click icon_next so show error
                    else if (!(otpWrapper.getEditText().getText().toString().matches("[0-9]+") && otpWrapper.getEditText().getText().toString().length() == 4)) {
                        otpWrapper.setError(getString(R.string.error_incorrect_otp_entered));
                    }
                    // User entered correct OTP and he need to be redirected to reset password page
                    else{
                        newPassWrapper.setVisibility(View.VISIBLE);
                        repeatPassWrapper.setVisibility(View.VISIBLE);
                        userNameWrapper.setVisibility(View.GONE);
                        otpWrapper.setVisibility(View.GONE);
                        btCustomButtonTextButton.setText(R.string.button_reset_label);
                        btBoarderless.setVisibility(View.GONE);
                        tvDidnotgetOTPLabel.setVisibility(View.GONE);
                    }
                }
            }
        });

        // Boarderless button (i.e., Forgot Pass, Send OTP Again) click listener
        btBoarderless.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // If password field is visible the user is on login page and if user forgot password then providing option for password reset.
                if(passwordWrapper.getVisibility()==View.VISIBLE){
                    passwordWrapper.setVisibility(View.GONE);
                    btBoarderless.setVisibility(View.GONE);
                    tvResetPassLabel.setVisibility(View.VISIBLE);
                    btCustomButtonTextButton.setText(R.string.button_submit_label);
                    userNameWrapper.setError("");
                }
                // If OTP field is visible user needs to enter correct OTP but if OTP is not received then providing option for sending OTP again.
                else if(otpWrapper.getVisibility()==View.VISIBLE){
                    Message.showShortSnack(v, "Resend OTP");
                }
            }
        });


        return rootView;
    }

}
