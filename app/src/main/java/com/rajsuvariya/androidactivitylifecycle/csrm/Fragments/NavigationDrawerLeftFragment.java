package com.rajsuvariya.androidactivitylifecycle.csrm.Fragments;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;


import com.rajsuvariya.androidactivitylifecycle.csrm.AccountDetailsActivity;
import com.rajsuvariya.androidactivitylifecycle.csrm.BaseActivity;
import com.rajsuvariya.androidactivitylifecycle.csrm.Helper.Message;
import com.rajsuvariya.androidactivitylifecycle.csrm.HomeActivity;
import com.rajsuvariya.androidactivitylifecycle.csrm.MainActivity;
import com.rajsuvariya.androidactivitylifecycle.csrm.QuickInsureActivity;
import com.rajsuvariya.androidactivitylifecycle.csrm.R;
import com.rajsuvariya.androidactivitylifecycle.csrm.RegistrationActivity;


import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by kuliza-319 on 6/2/17.
 */


public class NavigationDrawerLeftFragment extends Fragment {


    @BindView(R.id.nav_drawer_left_listview) ListView listView;
    @BindView(R.id.iv_nav_drawer_left_close_icon) ImageView mImageViewClose;
    @BindView(R.id.iv_nav_drawer_left_user_icon) ImageView mImageViewUser;
    @BindView(R.id.tv_nav_drawer_left_login_label) TextView mTextViewLogin;
    @BindView(R.id.tv_nav_drawer_left_register_label) TextView mTextViewRegister;
    @BindView(R.id.ll_nav_drawer_left_unidentified_user_options_wrapper) LinearLayout mLinearLayoutUnidentifiedUserOptionsWrapper;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_navigation_drawer_left, container, false);


        ButterKnife.bind(this, rootView);

        if(((BaseActivity)getContext()).isUserLoggedIn()){
            mLinearLayoutUnidentifiedUserOptionsWrapper.setVisibility(View.GONE);
        }else{
            mLinearLayoutUnidentifiedUserOptionsWrapper.setVisibility(View.VISIBLE);
        }

        listView.setAdapter(new MyArrayAdapter(getContext(), android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.nav_drawer_left_list_items)));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                closeButtonClick();
                switch (position){
                    case 0:
                        if(getActivity() instanceof HomeActivity){
                            break;
                        }
                        Intent homeActivity = new Intent(getContext(), HomeActivity.class);
                        startActivity(homeActivity);
                        break;
                    case 1:
//                        if(getActivity() instanceof AccountDetailsActivity){
//                            break;
//                        }
//                        Intent acActivity = new Intent(getContext(), AccountDetailsActivity.class);
//                        startActivity(acActivity);
                        Message.showShortToast(getContext(), ((MyArrayAdapter)listView.getAdapter()).getItem(position));
                        break;
                    case 2:
                        if(getActivity() instanceof QuickInsureActivity){
                            break;
                        }
                        Intent quickInsureActivity = new Intent(getContext(), QuickInsureActivity.class);
                        startActivity(quickInsureActivity);
                        break;
                    case 3:
//                        if(getActivity() instanceof RegistrationActivity){
//                            break;
//                        }
//                        Intent regActivity = new Intent(getContext(), RegistrationActivity.class);
//                        startActivity(regActivity);
                        Message.showShortToast(getContext(), ((MyArrayAdapter)listView.getAdapter()).getItem(position));
                        break;
                    case 4:
//                        if(getActivity() instanceof HomeActivity){
//                            break;
//                        }
                        Message.showShortToast(getContext(), ((MyArrayAdapter)listView.getAdapter()).getItem(position));
                        break;
                    case 5:
//                        if(getActivity() instanceof HomeActivity){
//                            break;
//                        }
                        Message.showShortToast(getContext(), ((MyArrayAdapter)listView.getAdapter()).getItem(position));
                        break;
                    case 6:
//                        if(getActivity() instanceof HomeActivity){
//                            break;
//                        }
                        Message.showShortToast(getContext(), ((MyArrayAdapter)listView.getAdapter()).getItem(position));
                        break;
                }
            }
        });




        return rootView;
    }


    public class MyArrayAdapter extends ArrayAdapter<String>{


        private int selectedItem;
        private String[] mObjects;


        public MyArrayAdapter(Context context, int resource, String[] objects) {
            super(context, resource, objects);
            this.mObjects = objects;


            if (getActivity() instanceof HomeActivity){
                selectedItem=0;
            }
            if (getActivity() instanceof AccountDetailsActivity){
                selectedItem=1;
            }
            if (getActivity() instanceof QuickInsureActivity){
                selectedItem=2;
            }
            if (getActivity() instanceof RegistrationActivity){
                selectedItem=3;
            }




        }

        public String getItem(int position){
            return mObjects[position];
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = super.getView(position, convertView, parent);
            Typeface font = Typeface.createFromAsset(getContext().getAssets(), "fonts/sohogothicproregular.otf");
            ((TextView)convertView).setTypeface(font, position == selectedItem ? Typeface.BOLD : Typeface.NORMAL);


            TextView tv = (TextView) convertView.findViewById(android.R.id.text1);
            tv.setTextColor(Color.WHITE);


            return convertView;
        }
    }


    @OnClick(R.id.iv_nav_drawer_left_close_icon)
    public void closeButtonClick(){
        ((BaseActivity)getContext()).closeNavDrawer(GravityCompat.START);
    }


    @OnClick(R.id.tv_nav_drawer_left_login_label)
    public void openLoginPage(){
        closeButtonClick();
        Intent i = new Intent(getContext(), MainActivity.class);
        startActivity(i);
    }


    @OnClick(R.id.tv_nav_drawer_left_register_label)
    public void openRegistrationPage(){
        closeButtonClick();
        Intent i = new Intent(getContext(), RegistrationActivity.class);
        startActivity(i);
    }

}


