package com.rajsuvariya.androidactivitylifecycle.csrm.Objects;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

/**
 * Created by kuliza-319 on 3/2/17.
 */

public class ERVQuickInsuranceGroupObject extends ExpandableGroup<ERVQuickInsuranceObject> {

    public ERVQuickInsuranceGroupObject(String title, List<ERVQuickInsuranceObject> items) {
        super(title, items);
    }


}