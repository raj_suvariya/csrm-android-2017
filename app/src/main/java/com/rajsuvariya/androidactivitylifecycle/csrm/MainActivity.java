package com.rajsuvariya.androidactivitylifecycle.csrm;

import android.support.design.widget.TabLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;

import com.rajsuvariya.androidactivitylifecycle.csrm.Adapter.FragmentAdapter;
import com.rajsuvariya.androidactivitylifecycle.csrm.Fragments.LoginFragment;
import com.rajsuvariya.androidactivitylifecycle.csrm.Fragments.SignUpFragment;
import com.rajsuvariya.androidactivitylifecycle.csrm.Helper.Message;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity {

    @BindView(R.id.viewpager_main) ViewPager mViewPager;
    @BindView(R.id.tablayout_main) TabLayout mTabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Modifying base view
        super.setChildContentView(R.layout.activity_main);
        super.disableNavDrawer(GravityCompat.START);
        super.disableNavDrawer(GravityCompat.END);

        // Binding views with butterknife
        ButterKnife.bind(this);

        // getSupportActionBar().setHomeAsUpIndicator(R.drawable.icon_home);

        // Setting up viewpager with the fragments
        setupViewPager();

        // Setting up mTabLayout with viewpager fragments titles
        mTabLayout.setupWithViewPager(mViewPager);

    }

    // adding fragments to adapter and seeting up the adapter to viewpager
    public void setupViewPager() {
        FragmentAdapter adapter = new FragmentAdapter(getSupportFragmentManager());
        adapter.addFragment(new LoginFragment(), "login");
        adapter.addFragment(new SignUpFragment(), "SignUp");
        mViewPager.setAdapter(adapter);
    }


    // Textwatcher class for implementing some actions when text in edit text changes
    public static class MyTextWatcher implements TextWatcher {

        TextInputLayout mTextInputLayout;

        public MyTextWatcher(TextInputLayout mTextInputLayout) {
            this.mTextInputLayout = mTextInputLayout;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        // On text change remove the error
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            mTextInputLayout.setError("");
            mTextInputLayout.setErrorEnabled(false);
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            Message.showShortToast(this, "Home Clicked!");
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }
}


