package com.rajsuvariya.androidactivitylifecycle.csrm.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kuliza-319 on 30/1/17.
 */

public class FragmentAdapter extends FragmentPagerAdapter {
    List<Fragment> mFragmentList = new ArrayList<>();
    List<String> mFragmentTitleList = new ArrayList<>();

    // Constructor
    public FragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    // Get fragment by position
    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    // Get fragment/page title by position
    public String getPageTitle(int position){
        return mFragmentTitleList.get(position);
    }

    // Add fragment to adapter
    public void addFragment(Fragment fragment, String title){
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

    // Get count of total fragments in adapter
    @Override
    public int getCount() {
        return mFragmentList.size();
    }

}
