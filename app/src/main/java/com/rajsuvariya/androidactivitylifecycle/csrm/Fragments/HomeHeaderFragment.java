package com.rajsuvariya.androidactivitylifecycle.csrm.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rajsuvariya.androidactivitylifecycle.csrm.Helper.Message;
import com.rajsuvariya.androidactivitylifecycle.csrm.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeHeaderFragment extends Fragment {

    private int mImageId;
    private String mButtonText;
    private String mBannerText;

    @BindView(R.id.home_header_viewpager_background_image) View mViewBackground;
    @BindView(R.id.home_header_fragment_text) TextView mTextViewLabel;
    @BindView(R.id.custombutton_layout_button) Button mButtonKnowMore;
    @BindView(R.id.home_header_fragment_button) View mButtonWrapper;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home_header, container, false);
        ButterKnife.bind(this, rootView);
        mViewBackground.setBackgroundResource(mImageId);

        if (mButtonText==null){
            mButtonWrapper.setVisibility(View.GONE);
            final float density = getResources().getDisplayMetrics().density;
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams
                    ((int) RelativeLayout.LayoutParams.WRAP_CONTENT, (int) RelativeLayout.LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.CENTER_VERTICAL);
            params.setMargins((int)(50*density),0, (int)(50*density), 0);
            mTextViewLabel.setLayoutParams(params);
        }
        else {
            mButtonKnowMore.setText(mButtonText);
        }
        mTextViewLabel.setText(mBannerText);

        mButtonKnowMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Message.showShortToast(container.getContext(), mTextViewLabel.getText().toString()+" Clicked !");

            }
        });
        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    public void setData(int imageId, String buttonText, String bannerText){
        this.mImageId = imageId;
        this.mButtonText = buttonText;
        this.mBannerText = bannerText;


    }

}
