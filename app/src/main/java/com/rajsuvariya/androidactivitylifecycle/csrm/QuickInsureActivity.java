package com.rajsuvariya.androidactivitylifecycle.csrm;

import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.rajsuvariya.androidactivitylifecycle.csrm.Adapter.CustomExpandableRecyclerViewQuickInsureAdapter;
import com.rajsuvariya.androidactivitylifecycle.csrm.Adapter.FragmentAdapter;
import com.rajsuvariya.androidactivitylifecycle.csrm.Fragments.HomeHeaderFragment;
import com.rajsuvariya.androidactivitylifecycle.csrm.Objects.ERVQuickInsuranceGroupObject;
import com.rajsuvariya.androidactivitylifecycle.csrm.Objects.ERVQuickInsuranceObject;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class QuickInsureActivity extends BaseActivity {

    @BindView(R.id.viewpager_quick_insure_top) ViewPager mViewPager;
    @BindView(R.id.vpindicator_quick_insure) CirclePageIndicator mViewPagerIndicator;
    @BindView(R.id.bt_quick_insure_top_viewpager_prev) ImageView mButtonPrevViewPager;
    @BindView(R.id.bt_quick_insure_top_viewpager_next) ImageView mButtonNextViewPager;
    @BindView(R.id.rv_quick_insurance_plans) RecyclerView mRecyclerViewQuickInsurancePlans;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setChildContentView(R.layout.activity_quick_insure);
        super.setLeftActionBarIcon(R.drawable.icon_menu);

        ButterKnife.bind(this);

        // Setting up viewpager (at top)
        setupViewPager();

        // Setting up circle page indicator
        setupCricleViewPagerIndicator();

        List<ERVQuickInsuranceGroupObject> go = new ArrayList<>();
        List<ERVQuickInsuranceObject> li = new ArrayList<>();
        li.add(new ERVQuickInsuranceObject(getString(R.string.lorem_ipsum_title_text), getString(R.string.lorem_ipsum_description_text), getString(R.string.buy_now_button_label), R.drawable.viewpager_image2));
        li.add(new ERVQuickInsuranceObject(getString(R.string.lorem_ipsum_title_text), getString(R.string.lorem_ipsum_description_text), getString(R.string.buy_now_button_label), R.drawable.viewpager_image3));
        li.add(new ERVQuickInsuranceObject(getString(R.string.lorem_ipsum_title_text), getString(R.string.lorem_ipsum_description_text), getString(R.string.buy_now_button_label), R.drawable.viewpager_image4));
        go.add(new ERVQuickInsuranceGroupObject("Plan 1", li));
        go.add(new ERVQuickInsuranceGroupObject("Plan 2", li));
        go.add(new ERVQuickInsuranceGroupObject("Plan 3", li));
        go.add(new ERVQuickInsuranceGroupObject("Plan 4", li));
        go.add(new ERVQuickInsuranceGroupObject("Plan 5", li));

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        CustomExpandableRecyclerViewQuickInsureAdapter adapter = new CustomExpandableRecyclerViewQuickInsureAdapter(go);
        mRecyclerViewQuickInsurancePlans.setLayoutManager(layoutManager);
        mRecyclerViewQuickInsurancePlans.setAdapter(adapter);

//        expandGroup(adapter, 0);
//        expandGroup(adapter, 1);
//        expandGroup(adapter, 2);

    }

//    public void expandGroup(CustomExpandableRecyclerViewQuickInsureAdapter adapter, int gpos){
//        if (adapter.isGroupExpanded(gpos)){
//            return;
//        }
//        adapter.toggleGroup(gpos);
//    }

    private void setupViewPager() {
        FragmentAdapter adapter = new FragmentAdapter(getSupportFragmentManager());
        HomeHeaderFragment first = new HomeHeaderFragment();
        HomeHeaderFragment second = new HomeHeaderFragment();
        HomeHeaderFragment third = new HomeHeaderFragment();
        HomeHeaderFragment fourth = new HomeHeaderFragment();
        HomeHeaderFragment fifth = new HomeHeaderFragment();

        first.setData(R.drawable.viewpager_image0, null, getString(R.string.redeem_offer_label1));
        second.setData(R.drawable.viewpager_image1, null, getString(R.string.redeem_offer_label2));
        third.setData(R.drawable.viewpager_image2, null, getString(R.string.redeem_offer_label3));
        fourth.setData(R.drawable.viewpager_image3, null, getString(R.string.redeem_offer_label1));
        fifth.setData(R.drawable.banner_registration_page, null, getString(R.string.redeem_offer_label2));

        adapter.addFragment(first, "First");
        adapter.addFragment(second, "Second");
        adapter.addFragment(third, "Third");
        adapter.addFragment(fourth, "Fourth");
        adapter.addFragment(fifth, "Fifth");

        mViewPager.setAdapter(adapter);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if(position!=0){
                    mButtonPrevViewPager.setVisibility(View.VISIBLE);
                }
                else{
                    mButtonPrevViewPager.setVisibility(View.GONE);
                }
                if(position!=(mViewPager.getAdapter().getCount()-1)){
                    mButtonNextViewPager.setVisibility(View.VISIBLE);
                }
                else{
                    mButtonNextViewPager.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }

    // onClickListener for previous button on viewpager
    @OnClick(R.id.bt_quick_insure_top_viewpager_prev)
    public void prevHeaderButtonClick(){
        if (mViewPager.getCurrentItem()==0){
            mViewPager.setCurrentItem(mViewPager.getAdapter().getCount()-1, true);
        }
        else {
            mViewPager.setCurrentItem((mViewPager.getCurrentItem() - 1) % mViewPager.getAdapter().getCount(), true);
        }
    }

    // onClickListener for icon_next button on viewpager
    @OnClick(R.id.bt_quick_insure_top_viewpager_next)
    public void nextHeaderButtonClick(){
        mViewPager.setCurrentItem((mViewPager.getCurrentItem()+1)% mViewPager.getAdapter().getCount(), true);
    }


    private void setupCricleViewPagerIndicator() {
        mViewPagerIndicator.setViewPager(mViewPager);
        final float density = getResources().getDisplayMetrics().density;
        mViewPagerIndicator.setRadius(5 * density);
    }
}
