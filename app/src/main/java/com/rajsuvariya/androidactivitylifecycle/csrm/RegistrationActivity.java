package com.rajsuvariya.androidactivitylifecycle.csrm;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v4.view.GravityCompat;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.rajsuvariya.androidactivitylifecycle.csrm.Helper.Message;
import com.rajsuvariya.androidactivitylifecycle.csrm.Helper.Validator;

import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RegistrationActivity extends BaseActivity {

    @BindView(R.id.tv_registration_register_label) TextView tvRegisterLabel;
    @BindView(R.id.registration_first_name_wrapper) TextInputLayout firstNameWrapper;
    @BindView(R.id.registration_last_name_wrapper) TextInputLayout lastNameWrapper;
    @BindView(R.id.registration_email_wrapper) TextInputLayout emailIdWrapper;
    @BindView(R.id.registration_mobile_no_wrapper) TextInputLayout mobileNoWrapper;
    @BindView(R.id.registration_pincode_wrapper) TextInputLayout pinCodeWrapper;
    @BindView(R.id.registration_create_password_wrapper) TextInputLayout createPassWrapper;
    @BindView(R.id.registration_confirm_password_wrapper) TextInputLayout confirmPassWrapper;
    @BindView(R.id.registration_dob_wrapper) TextInputLayout dateOfBirthWrapper;
    @BindView(R.id.registration_address_line1_wrapper) TextInputLayout addressLine1Wrapper;
    @BindView(R.id.registration_address_line2_wrapper) TextInputLayout addressLine2Wrapper;
    @BindView(R.id.registration_address_landmark_wrapper) TextInputLayout addressLandmarkWrapper;
    @BindView(R.id.registration_address_city_wrapper) TextInputLayout addressCityWrapper;
    @BindView(R.id.registration_state_wrapper) TextInputLayout stateWrapper;


    @BindView(R.id.bt_registration_save_layout) View btCustomSaveLayout;
    @BindView(R.id.custombutton_layout_button) Button btCustomSaveTextButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Modifying base view
        super.setChildContentView(R.layout.activity_registration);
        super.disableNavDrawer(GravityCompat.START);
        super.disableNavDrawer(GravityCompat.END);

        // Binding views
        ButterKnife.bind(this);

//        getSupportActionBar().setHomeAsUpIndicator(R.drawable.icon_home);

        // Customize the button
        btCustomSaveTextButton.setText(R.string.button_save_label);

        // Set Text change listeners to all the text wrappers
        setTextChangeListeners();

        // Add on click listener on state field to open pop up to choose state
        stateWrapper.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MaterialDialog.Builder(RegistrationActivity.this)
                        .title("Please select state")
                        .items(R.array.india_states)
                        .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                            @Override
                            public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                stateWrapper.getEditText().setText(text);
                                return true;
                            }
                        })
                        .positiveText("Choose")
                        .show();
            }
        });

        // Add on click listener on birthday field to open pop up to choose birthdate
        dateOfBirthWrapper.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(999);
            }
        });

        // When clicked save validate all the input and if valid then call the registration API
        btCustomSaveLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isValidRegistration = true;
                String firstName = firstNameWrapper.getEditText().getText().toString();
                String lastName = lastNameWrapper.getEditText().getText().toString();
                String emailId = emailIdWrapper.getEditText().getText().toString();
                String mobileNo = mobileNoWrapper.getEditText().getText().toString();
                String dateofBirth = dateOfBirthWrapper.getEditText().getText().toString();
                String pinCode = pinCodeWrapper.getEditText().getText().toString();
                String createPass = createPassWrapper.getEditText().getText().toString();
                String confirmPass = confirmPassWrapper.getEditText().getText().toString();
                String addressLine1 = addressLine1Wrapper.getEditText().getText().toString();
                String addressLine2 = addressLine2Wrapper.getEditText().getText().toString();
                String addressLandmark = addressLandmarkWrapper.getEditText().getText().toString();
                String addressCity = addressCityWrapper.getEditText().getText().toString();
                String state = stateWrapper.getEditText().getText().toString();

                if (TextUtils.isEmpty(firstName)){
                    firstNameWrapper.setError(getString(R.string.error_no_firstname_entered));
                    isValidRegistration=false;
                }
                else if(!Validator.isFirstNameValid(firstName)){
                    firstNameWrapper.setError(getString(R.string.error_incorrect_first_name_entered));
                    isValidRegistration=false;
                }
                if (TextUtils.isEmpty(lastName)){
                    lastNameWrapper.setError(getString(R.string.error_no_lastname_entered));
                    isValidRegistration=false;
                }
                if (TextUtils.isEmpty(emailId)){
                    emailIdWrapper.setError(getString(R.string.error_no_emailid_entered));
                    isValidRegistration=false;
                }
                else if(!Validator.isValidEmail(emailId)){
                    emailIdWrapper.setError(getString(R.string.error_incorrect_emailid_entered));
                    isValidRegistration=false;
                }
                if (TextUtils.isEmpty(mobileNo)){
                    mobileNoWrapper.setError(getString(R.string.error_no_mobileno_entered));
                    isValidRegistration=false;
                }
                else if(!Validator.isValidMobileNumber(mobileNo)){
                    mobileNoWrapper.setError(getString(R.string.error_incorrect_mobileno_entered));
                    isValidRegistration=false;
                }
                if(TextUtils.isEmpty(dateofBirth)){
                    dateOfBirthWrapper.setError(getString(R.string.error_no_dob_entered));
                    isValidRegistration=false;
                }
                if(TextUtils.isEmpty(addressLine1)){
                    addressLine1Wrapper.setError(getString(R.string.error_no_address_line_1_entered));
                    isValidRegistration=false;
                }
                else if(!Validator.isAddressFirstLineValid(addressLine1)){
                    addressLine1Wrapper.setError(getString(R.string.error_incorrect_address_entered));
                    isValidRegistration=false;
                }
                if(TextUtils.isEmpty(addressLandmark)){
                    addressLandmarkWrapper.setError(getString(R.string.error_no_address_landmark_entered));
                    isValidRegistration=false;
                }
//                else if(Validator.is___(addressLandmark)){
//                    addressLandmarkWrapper.setError(getString(R.string.error_incorrect_address_entered));
//                    isValidRegistration=false;
//                }
                if(TextUtils.isEmpty(addressCity)){
                    addressCityWrapper.setError(getString(R.string.error_no_address_city_entered));
                    isValidRegistration=false;
                }
//                else if(Validator.is___(addressCity)){
//                    addressCityWrapper.setError(getString(R.string.error_incorrect_address_entered));
//                    isValidRegistration=false;
//                }
                if (TextUtils.isEmpty(state)){
                    stateWrapper.setError(getString(R.string.error_no_state_entered));
                    isValidRegistration=false;
                }

                if (TextUtils.isEmpty(pinCode)){
                    pinCodeWrapper.setError(getString(R.string.error_no_pincode_entered));
                    isValidRegistration=false;
                }
                else if(!Validator.isValidPinCode(pinCode)){
                    pinCodeWrapper.setError(getString(R.string.error_incorrect_pincode_entered));
                    isValidRegistration=false;
                }
                if (TextUtils.isEmpty(createPass)){
                    createPassWrapper.setError(getString(R.string.error_no_password_entered));
                    isValidRegistration=false;
                }
                else if(!Validator.isPasswordValid(createPass)){
                    createPassWrapper.setError(getString(R.string.error_incorrect_password_entered));
                    isValidRegistration=false;
                }
                if (TextUtils.isEmpty(confirmPass)){
                    confirmPassWrapper.setError(getString(R.string.error_no_password_entered));
                    isValidRegistration=false;
                }
                else if(!createPass.equals(confirmPass)){
                    confirmPassWrapper.setError(getString(R.string.error_passwords_do_not_match));
                    isValidRegistration=false;
                }
                if (isValidRegistration){
                    Message.showShortSnack(v, "Call API");
                    Intent i = new Intent(getApplicationContext(), HomeActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                }
            }
        });

    }

    private void setTextChangeListeners() {
        firstNameWrapper.getEditText().addTextChangedListener(new MainActivity.MyTextWatcher(firstNameWrapper));
        lastNameWrapper.getEditText().addTextChangedListener(new MainActivity.MyTextWatcher(lastNameWrapper));
        emailIdWrapper.getEditText().addTextChangedListener(new MainActivity.MyTextWatcher(emailIdWrapper));
        mobileNoWrapper.getEditText().addTextChangedListener(new MainActivity.MyTextWatcher(mobileNoWrapper));
        pinCodeWrapper.getEditText().addTextChangedListener(new MainActivity.MyTextWatcher(pinCodeWrapper));
        createPassWrapper.getEditText().addTextChangedListener(new MainActivity.MyTextWatcher(createPassWrapper));
        confirmPassWrapper.getEditText().addTextChangedListener(new MainActivity.MyTextWatcher(confirmPassWrapper));
        addressLine1Wrapper.getEditText().addTextChangedListener(new MainActivity.MyTextWatcher(addressLine1Wrapper));
        addressLine2Wrapper.getEditText().addTextChangedListener(new MainActivity.MyTextWatcher(addressLine2Wrapper));
        addressLandmarkWrapper.getEditText().addTextChangedListener(new MainActivity.MyTextWatcher(addressLandmarkWrapper));
        addressCityWrapper.getEditText().addTextChangedListener(new MainActivity.MyTextWatcher(addressCityWrapper));
        dateOfBirthWrapper.getEditText().addTextChangedListener(new MainActivity.MyTextWatcher(dateOfBirthWrapper));
        stateWrapper.getEditText().addTextChangedListener(new MainActivity.MyTextWatcher(stateWrapper));
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        if (id == 999) {
            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePickerDialog = new DatePickerDialog(this, myDateListener, year, month, day);
            datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
            return datePickerDialog;
        }
        return super.onCreateDialog(id);
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new
            DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0,
                                      int arg1, int arg2, int arg3) {
                    Map<Integer, String> monthNames = new HashMap<>();
                    DateFormatSymbols dfs = new DateFormatSymbols();
                    String[] months = dfs.getMonths();
                    dateOfBirthWrapper.getEditText().setText(new StringBuilder().append(arg3).append(" ")
                            .append(months[arg2]).append(" ").append(arg1));
                }
            };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

}
