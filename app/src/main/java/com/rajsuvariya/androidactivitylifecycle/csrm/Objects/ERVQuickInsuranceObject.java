package com.rajsuvariya.androidactivitylifecycle.csrm.Objects;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kuliza-319 on 3/2/17.
 */

public class ERVQuickInsuranceObject implements Parcelable {

    private String mInsuranceTitle;
    private String mInsuranceDescription;
    private String mInsuranceButtonText;
    private int mInsuranceImageId;

    public ERVQuickInsuranceObject(String mInsuranceTitle, String mInsuranceDescription, String mInsuranceButtonText, int mInsuranceImageId) {
        this.mInsuranceTitle = mInsuranceTitle;
        this.mInsuranceDescription = mInsuranceDescription;
        this.mInsuranceButtonText = mInsuranceButtonText;
        this.mInsuranceImageId = mInsuranceImageId;
    }

    protected ERVQuickInsuranceObject(Parcel in) {
        this.mInsuranceTitle = in.readString();
        this.mInsuranceDescription = in.readString();
        this.mInsuranceButtonText = in.readString();
        this.mInsuranceImageId = in.readInt();
    }

    public String getInsuranceTitle() {
        return mInsuranceTitle;
    }

    public void setInsuranceTitle(String mInsuranceTitle) {
        this.mInsuranceTitle = mInsuranceTitle;
    }

    public String getInsuranceDescription() {
        return mInsuranceDescription;
    }

    public void setInsuranceDescription(String mInsuranceDescription) {
        this.mInsuranceDescription = mInsuranceDescription;
    }

    public String getInsuranceButtonText() {
        return mInsuranceButtonText;
    }

    public void setInsuranceButtonText(String mInsuranceButtonText) {
        this.mInsuranceButtonText = mInsuranceButtonText;
    }

    public int getInsuranceImageId() {
        return mInsuranceImageId;
    }

    public void setInsuranceImageId(int mInsuranceImageId) {
        this.mInsuranceImageId = mInsuranceImageId;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mInsuranceTitle);
        dest.writeString(mInsuranceDescription);
        dest.writeString(mInsuranceButtonText);
        dest.writeInt(mInsuranceImageId);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public final Creator<ERVQuickInsuranceObject> CREATOR = new Creator<ERVQuickInsuranceObject>() {
        @Override
        public ERVQuickInsuranceObject createFromParcel(Parcel in) {
            return new ERVQuickInsuranceObject(in);
        }

        @Override
        public ERVQuickInsuranceObject[] newArray(int size) {
            return new ERVQuickInsuranceObject[size];
        }
    };
}
