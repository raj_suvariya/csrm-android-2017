package com.rajsuvariya.androidactivitylifecycle.csrm.Fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.rajsuvariya.androidactivitylifecycle.csrm.AccountDetailsActivity;
import com.rajsuvariya.androidactivitylifecycle.csrm.AccountSettingsActivity;
import com.rajsuvariya.androidactivitylifecycle.csrm.BaseActivity;
import com.rajsuvariya.androidactivitylifecycle.csrm.HomeActivity;
import com.rajsuvariya.androidactivitylifecycle.csrm.MainActivity;
import com.rajsuvariya.androidactivitylifecycle.csrm.QuickInsureActivity;
import com.rajsuvariya.androidactivitylifecycle.csrm.R;
import com.rajsuvariya.androidactivitylifecycle.csrm.RegistrationActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by kuliza-319 on 6/2/17.
 */

public class NavigationDrawerRightFragment extends Fragment {

    @BindView(R.id.nav_drawer_right_listview) ListView listView;
    @BindView(R.id.iv_nav_drawer_right_close_icon) ImageView mImageViewClose;
    @BindView(R.id.iv_nav_drawer_right_user_icon) ImageView mImageViewUser;
    @BindView(R.id.tv_nav_drawer_right_name) TextView mTextViewName;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_navigation_drawer_right, container, false);

        ButterKnife.bind(this, rootView);

        listView.setAdapter(new MyArrayAdapter(getContext(), android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.nav_drawer_right_list_items)));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                closeButtonClick();
                switch (position){
                    case 0:
                        if(getActivity() instanceof AccountDetailsActivity){
                            break;
                        }
                        Intent accountDetailsActivity = new Intent(getContext(), AccountDetailsActivity.class);
                        startActivity(accountDetailsActivity);
                        break;
                    case 1:
                        if(getActivity() instanceof AccountSettingsActivity){
                            break;
                        }
                        Intent accountSettingsActivity = new Intent(getContext(), AccountSettingsActivity.class);
                        startActivity(accountSettingsActivity);
                        break;
                    case 2:
                        SharedPreferences pref = getContext().getSharedPreferences("CSRMPref", MODE_PRIVATE);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putBoolean("isUserLoggedIn", false);
                        editor.commit();
                        Intent i = new Intent(getContext(), HomeActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        break;
                    default:
//                        Intent mainActivity = new Intent(getContext(), HomeActivity.class);
//                        startActivity(mainActivity);
//                        getActivity().finish();
                        break;
                }
            }
        });

        return rootView;
    }

    public class MyArrayAdapter extends ArrayAdapter<String>{

        private int selectedItem;

        public MyArrayAdapter(Context context, int resource, String[] objects) {
            super(context, resource, objects);

            if (getActivity() instanceof AccountDetailsActivity){
                selectedItem=0;
            }
            if (getActivity() instanceof AccountDetailsActivity){
                selectedItem=1;
            }
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = super.getView(position, convertView, parent);
            Typeface font = Typeface.createFromAsset(getContext().getAssets(), "fonts/sohogothicproregular.otf");
            ((TextView)convertView).setTypeface(font, position == selectedItem ? Typeface.BOLD : Typeface.NORMAL);

            TextView tv = (TextView) convertView.findViewById(android.R.id.text1);
            tv.setTextColor(Color.WHITE);

            return convertView;
        }
    }

    @OnClick(R.id.iv_nav_drawer_right_close_icon)
    public void closeButtonClick(){
        ((BaseActivity)getContext()).closeNavDrawer(GravityCompat.END);
    }

}

