package com.rajsuvariya.androidactivitylifecycle.csrm.Helper;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.Toast;

import com.rajsuvariya.androidactivitylifecycle.csrm.Application;

/**
 * Created by kuliza-319 on 23/1/17.
 */

public class Message {

    // this class is for making quick toasts and snacks.

    private static Context mContext;

    public static void showShortSnack (View view, String message){
        Snackbar.make(view, message, Snackbar.LENGTH_SHORT).show();
    }

    public static void showLongSnack (View view, String message){
        Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();
    }

    public static void showShortToast (Context context, String message){
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static void showLongToast (Context context, String message){
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }
}
