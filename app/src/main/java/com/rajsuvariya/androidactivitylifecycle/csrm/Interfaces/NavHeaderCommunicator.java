package com.rajsuvariya.androidactivitylifecycle.csrm.Interfaces;

/**
 * Created by kuliza-319 on 6/2/17.
 */

public interface NavHeaderCommunicator {

    public void closeNavDrawer(int gravity);

}
