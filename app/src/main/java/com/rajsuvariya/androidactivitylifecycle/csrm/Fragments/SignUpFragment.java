package com.rajsuvariya.androidactivitylifecycle.csrm.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.rajsuvariya.androidactivitylifecycle.csrm.Helper.Message;
import com.rajsuvariya.androidactivitylifecycle.csrm.MainActivity;
import com.rajsuvariya.androidactivitylifecycle.csrm.R;
import com.rajsuvariya.androidactivitylifecycle.csrm.RegistrationActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by kuliza-319 on 23/1/17.
 */

public class SignUpFragment extends Fragment {

    @BindView(R.id.signup_username_wrapper) TextInputLayout userNameWrapper;
    @BindView(R.id.signup_otp_wrapper) TextInputLayout otpWrapper;
    @BindView(R.id.signup_layout_custom_button) View btCustomButtonLayout;
    @BindView(R.id.custombutton_layout_button) Button btCustomButtonTextButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_signup, container, false);

        // Binding views with ButterKnife
        ButterKnife.bind(this, rootView);

        // Changing the text of the custom button
        btCustomButtonTextButton.setText(getString(R.string.button_signup_label));

        // Setting textwatcher to all the input fields
        userNameWrapper.getEditText().addTextChangedListener(new MainActivity.MyTextWatcher(userNameWrapper));
        otpWrapper.getEditText().addTextChangedListener(new MainActivity.MyTextWatcher(otpWrapper));

        // On click listner for the custom button (Main Button, i.e., SignUp)
        btCustomButtonLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // When otp fied is not visible means the user have to enter valid ID
                if(otpWrapper.getVisibility()==View.GONE){
                    boolean isUsernameEmpty = TextUtils.isEmpty(userNameWrapper.getEditText().getText().toString());
                    if (isUsernameEmpty){  // Invalid ID so show error
                        userNameWrapper.setError(getString(R.string.error_incorrect_customer_id));
                    }
                    else{  // validate customer ID by calling API
                        otpWrapper.setVisibility(View.VISIBLE);
                        userNameWrapper.setEnabled(false);
                    }
                }
                // User received OTP so check the OTP
                else{
                    boolean isOTPEmpty =  TextUtils.isEmpty(otpWrapper.getEditText().getText().toString());
                    if (isOTPEmpty){  // User haven't entered OTP and click icon_next so show error
                        otpWrapper.setError(getString(R.string.error_no_otp_entered));
                    }
                    // Entered OTP cannot contain anything other then decimal numbers and length should be 4
                    else if (!(otpWrapper.getEditText().getText().toString().matches("[0-9]+") && otpWrapper.getEditText().getText().toString().length() == 4)) {
                        otpWrapper.setError(getString(R.string.error_incorrect_otp_entered));
                    }
                    // If OTP is correct then start the user registration
                    else{
                        Intent i = new Intent(getContext(), RegistrationActivity.class);
                        startActivity(i);
                    }
                }
            }
        });

        return rootView;
    }
}
