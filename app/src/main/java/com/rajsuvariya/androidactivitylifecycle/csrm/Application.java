package com.rajsuvariya.androidactivitylifecycle.csrm;

import com.rajsuvariya.androidactivitylifecycle.csrm.Helper.FontsOverride;

/**
 * Created by kuliza-319 on 24/1/17.
 */

public final class Application extends android.app.Application {
    @Override
    public void onCreate() {
        super.onCreate();

        // Changing fonts of entire app by overriding default fonts
        FontsOverride.setDefaultFont(this, "DEFAULT", "fonts/sohogothicproregular.otf");
        FontsOverride.setDefaultFont(this, "MONOSPACE", "fonts/sohogothicproregular.otf");

    }
}